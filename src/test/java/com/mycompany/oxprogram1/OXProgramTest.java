/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.oxprogram1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Acer
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckDrawResultIsDraw() {
        assertEquals(true, OXProgram.checkDraw(8));
    }

    @Test
    public void testCheckWinResultIsXWin() {
        char[][] table = {{'X', 'O', '-'}, {'X', 'O', '-'}, {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinResultIsOWin() {
        char[][] table = {{'O', 'O', 'X'}, {'X', 'O', '-'}, {'X', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testPositionTakenIsFalse() {
        char[][] table = {{'-', 'O', '-'}, {'X', 'O', '-'}, {'O', '-', '-'}};
        assertEquals(false, OXProgram.checkPositionTaken(table, 1, 1));
    }
    
    @Test
    public void testPositionTakenIsTrue() {
        char[][] table = {{'X', 'O', '-'}, {'X', 'O', '-'}, {'O', '-', '-'}};
        assertEquals(true, OXProgram.checkPositionTaken(table, 1, 1));
    }

    @Test
    public void testCheckOutOfRangeIsFalse() {
        int row = 1, col = 1;
        assertEquals(false, OXProgram.checkOutOfRange(row, col));
    }
    
    @Test
    public void testCheckOutOfRangeIsTrue() {
        int row = 4, col = 4;
        assertEquals(true, OXProgram.checkOutOfRange(row, col));
    }

    @Test
    public void testSetTableIsTrue() {
        char[][] table = {{'-', 'O', '-'}, {'X', '-', '-'}, {'O', '-', '-'}};
        int row = 1, col = 1;
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.setTable(table, row, col, currentPlayer));
    }
    
    @Test
    public void testSetTableIsFalse() {
        char[][] table = {{'X', 'O', '-'}, {'X', '-', '-'}, {'O', '-', '-'}};
        int row = 1, col = 1; // Position Taken.
        char currentPlayer = 'O';
        assertEquals(false, OXProgram.setTable(table, row, col, currentPlayer)); // Must be "false" because checkPositionTaken is "false".
    }
}
