/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.oxprogram1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Acer
 */
public class TDDTest {
    
    public TDDTest() {
    }

    // add(int a,int b) -> int
    // add(3, 4) --> 7
    @Test
    public void testAdd_1_2is3() {
        assertEquals(3, Example.add(1,2));
    }
    
    @Test
    public void testAdd_3_4is7() {
        assertEquals(7, Example.add(3,4));
    }
    
    @Test
    public void testChup_p1_P_p2_H_P1win() {
        assertEquals("P1 win",Example.chup('P','H'));
    }
    
    @Test
    public void testChup_p1_S_p2_H_P2win() {
        assertEquals("P2 win",Example.chup('S','H'));
    }
    
    @Test
    public void testChup_p1_S_p2_S_Draw() {
        assertEquals("Draw",Example.chup('S','S'));
    }
}