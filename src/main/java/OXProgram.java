/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class OXProgram {

    /**
     * @param args the command line arguments
     */
    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    public static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    public static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        showTable(table);
        while (true) {
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    public static void showTable(char table[][]) {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c] + " ");
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row, col:");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void process() {
        if (setTable(table, row, col, currentPlayer)) {
            if (checkWin(table, currentPlayer)) {
                finish = true;
                showWin();
                return;
            }
            if (checkDraw(count)) {
                finish = true;
                showDraw();
                return;
            }
            count++;
            switchPlayer();
        }
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    /*Pure*/
    public static boolean setTable(char[][] table, int row, int col, char currentPlayer) {
        if (checkOutOfRange(row, col) == false && checkPositionTaken(table, row, col) == false) {
            table[row - 1][col - 1] = currentPlayer;
            showTable(table);
            return true;
        }
        return false;
    }

    /*Pure*/
    public static boolean checkWin(char[][] table, char currentPlayer) {
        if (table[0][0] == currentPlayer && table[0][1] == currentPlayer && table[0][2] == currentPlayer) {
            return true;
        }
        if (table[1][0] == currentPlayer && table[1][1] == currentPlayer && table[1][2] == currentPlayer) {
            return true;
        }
        if (table[2][0] == currentPlayer && table[2][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        if (table[0][0] == currentPlayer && table[1][0] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        if (table[0][1] == currentPlayer && table[1][1] == currentPlayer && table[2][1] == currentPlayer) {
            return true;
        }
        if (table[0][2] == currentPlayer && table[1][2] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }

        return false;

    }

    public static void showWin() {
        System.out.println(">>>" + currentPlayer + " Win<<<");
    }

    /*Pure*/
    public static boolean checkDraw(int count) {
        if (count > 7) {
            return true;
        }
        return false;
    }

    public static void showDraw() {
        System.out.println(">>>Draw<<<");
    }

    /*Pure*/
    public static boolean checkPositionTaken(char[][] table, int row, int col) {
        if (table[row - 1][col - 1] != '-') {
            showTaken();
            return true;
        } else {
            return false;
        }
    }

    public static void showTaken() {
        System.out.println(">>>Position Taken<<<");
        showTable(table);
    }

    /*Pure*/
    public static boolean checkOutOfRange(int row, int col) {
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            showOutOfRange();
            return true;
        } else {
            return false;
        }
    }

    public static void showOutOfRange() {
        System.out.println(">>>Out Of Range<<<");
        showTable(table);
    }
}
